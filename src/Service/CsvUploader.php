<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class CsvUploader
{

    public function __construct()
    {
    }

    public function process(UploadedFile $file)
    {
        $data['success'] = false;
        try {
            if (strpos($file->getMimeType(), 'csv') === false && strpos($file->getMimeType(), 'plain') === false) {
                $data['message'] = 'Error: Only valid csv files are supported';
            }
            $csvData = $this->convertCsv($file->getRealPath(), ',');
            if($csvData){
            $data['csv'] = $csvData;
            }
            else{
                $data['message'] = 'Error in processing the CSV, please check that headers and rows are the same length';
            }
        } catch (FileException $e) {
            $data['message'] = $e->getMessage();
        }

        return $data;
    }

    public function convertCsv($filename, $delimiter = ',')
    {
        if (!file_exists($filename) || !is_readable($filename)) {
            return FALSE;
        }

        $header = NULL;
        $data = array();

        if (($handle = fopen($filename, 'r')) !== FALSE) {
            while (($row = fgetcsv($handle, 1000, $delimiter)) !== FALSE) {
                if (!$header) {
                    $header = $row;
                } else {
                    $data[] = array_combine($header, $row);
                }
            }
            fclose($handle);
        }
        return $data;
    }
}
