<?php

namespace App\Entity;

use App\Repository\InvoiceRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InvoiceRepository::class)
 * @ORM\HasLifecycleCallbacks()
 */
class Invoice
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $invoiceId;

    /**
     * @ORM\Column(type="bigint")
     */
    private $amount;

    /**
     * @ORM\Column(type="bigint")
     */
    private $sellingPrice;

    /**
     * @ORM\Column(type="date")
     */
    private $dueOn;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAmount(): ?string
    {
        return $this->amount;
    }

    public function setAmount(string $amount): self
    {
        $this->amount = $amount*100;

        return $this;
    }

    public function getDueOn(): ?\DateTimeInterface
    {
        return $this->dueOn;
    }

    public function setDueOn(\DateTimeInterface $dueOn): self
    {
        $this->dueOn = $dueOn;

        return $this;
    }

    public function getInvoiceId()
    {
        return $this->invoiceId;
    }

    public function setInvoiceId($invoiceId): self
    {
        $this->invoiceId = $invoiceId;

        return $this;
    }

    public function getSellingPrice(): string
    {
        return $this->sellingPrice;
    }

    /**
     * @ORM\PrePersist()
     **/

    public function setSellingPrice() :self
    {
        $coefficient = 0.3;
        $today = new \DateTime('now');
        $dateCondition = $today
            ->add(new \DateInterval('P30D'));

        if($dateCondition < $this->dueOn) {
            $coefficient = 0.5;
        }

        $this->sellingPrice = $this->amount * $coefficient;

        return $this;
    }
}
