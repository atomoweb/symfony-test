<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Form\Type\CsvType;
use App\Service\CsvUploader;
use App\Entity\Invoice;
use App\Repository\InvoiceRepository;

class InvoiceController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();
        $invoices = $em->getRepository('App:Invoice')->findAll();
        dump($invoices);
        $csvForm = $this->createForm(CsvType::class);
        return $this->render('invoice/index.html.twig', [
            'form' => $csvForm->createView(),
            'invoices' => $invoices,
        ]);
    }

    /**
     * @Route("/api/upload", name="api_upload")
     * @param Request $request
     * @param CsvUploader $csvUploader
     * @return JsonResponse
     */
    public function csvUpload(Request $request, CsvUploader $csvUploader)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(CsvType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var UploadedFile $csvFile */
            $csvFile = $form['import']->getData();
            if ($csvFile) {
                $data = $csvUploader->process($csvFile);
                if(isset($data) && isset($data['csv'])){
                    $dataCsv = $data['csv'];
                    $data['logs'][] = 'starting';
                    $count = 0;
                    $countErrors = 0;
                    //dump($dataCsv);
                    foreach($dataCsv as $row){
                        $count = $count + 1;
                        $invoice = new Invoice();
                        $data['logs'][] = 'row '.$count. ' processing';
                        if(!isset($row['internal invoice id'])){
                            $data['errors'][] = 'internal invoice id not found in row '.$count;
                            $countErrors = $countErrors + 1;
                            continue;
                        }
                        elseif(!isset($row['amount'])){
                            $data['errors'][] = 'amount not found in row '.$count;
                            $countErrors = $countErrors + 1;
                            continue;
                        }
                        elseif(!isset($row['due on'])){
                            $data['errors'][] = 'due on not found in row '.$count;
                            $countErrors = $countErrors + 1;
                            continue;
                        }
                        $invoiceId = intval($row['internal invoice id']);
                        $invoice->setInvoiceId($invoiceId);
                        $amount = floatval($row['amount']);
                        $invoice->setAmount($amount);
                        $dueOn = new \DateTime($row['due on']);
                        $invoice->setDueOn($dueOn);
                        $em->persist($invoice);
                        $em->flush();
                    }
                    $data['success'] = true;
                    $data['message'] = $countErrors?'Completed with errors':'Completed without errors';

                }
                else{
                    if(!isset($data)) {
                        $data['success'] = false;
                        $data['message'] = 'There was an error processing the file';
                    }
                }
            }
            else{
                $data['success'] = false;
                $data['message'] = 'Please select and upload a file';
            }

            }

        return new JsonResponse($data);
    }
}
